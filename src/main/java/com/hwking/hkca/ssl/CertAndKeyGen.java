package com.hwking.hkca.ssl;

import java.io.IOException;
import java.security.*;
import java.security.cert.X509Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateEncodingException;
import java.util.Date;
import sun.security.x509.*;

/**
 * 39    * Generate a pair of keys, and provide access to them.  This class is
 * 40    * provided primarily for ease of use.
 * 41    *
 * 42    * <P>This provides some simple certificate management functionality.
 * 43    * Specifically, it allows you to create self-signed X.509 certificates
 * 44    * as well as PKCS 10 based certificate signing requests.
 * 45    *
 * 46    * <P>Keys for some public key signature algorithms have algorithm
 * 47    * parameters, such as DSS/DSA.  Some sites' Certificate Authorities
 * 48    * adopt fixed algorithm parameters, which speeds up some operations
 * 49    * including key generation and signing.  <em>At this time, this interface
 * 50    * does not provide a way to provide such algorithm parameters, e.g.
 * 51    * by providing the CA certificate which includes those parameters.</em>
 * 52    *
 * 53    * <P>Also, note that at this time only signature-capable keys may be
 * 54    * acquired through this interface.  Diffie-Hellman keys, used for secure
 * 55    * key exchange, may be supported later.
 * 56    *
 * 57    * @author David Brownell
 * 58    * @author Hemma Prafullchandra
 * 59    * @see PKCS10
 * 60    * @see X509CertImpl
 * 61
 */
public final class CertAndKeyGen {
    public CertAndKeyGen(String keyType, String sigAlg) throws NoSuchAlgorithmException {
        keyGen = KeyPairGenerator.getInstance(keyType);
        this.sigAlg = sigAlg;
    }

    public CertAndKeyGen(String keyType, String sigAlg, String providerName) throws NoSuchAlgorithmException, NoSuchProviderException {
        if (providerName == null) {
            keyGen = KeyPairGenerator.getInstance(keyType);
        } else {
            try {
                keyGen = KeyPairGenerator.getInstance(keyType, providerName);
            } catch (Exception e) {
                // try first available provider instead
                keyGen = KeyPairGenerator.getInstance(keyType);
            }
        }
        this.sigAlg = sigAlg;
    }

    public void setRandom(SecureRandom generator) {
        prng = generator;
    }

    public void generate(int keyBits) throws InvalidKeyException {
        KeyPair pair;

        try {
            if (prng == null) {
                prng = new SecureRandom();
            }
            keyGen.initialize(keyBits, prng);
            pair = keyGen.generateKeyPair();

        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage());
        }

        publicKey = pair.getPublic();
        privateKey = pair.getPrivate();
    }

    public X509Key getPublicKey() {
        if (!(publicKey instanceof X509Key)) {
            return null;
        }
        return (X509Key) publicKey;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public X509Certificate getSelfCertificate(X500Name myname, Date firstDate, long validity) throws CertificateException, InvalidKeyException, SignatureException, NoSuchAlgorithmException, NoSuchProviderException {
        X509CertImpl cert;
        Date lastDate;

        try {
            lastDate = new Date();
            lastDate.setTime(firstDate.getTime() + validity * 1000);

            CertificateValidity interval = new CertificateValidity(firstDate, lastDate);

            X509CertInfo info = new X509CertInfo();
            // Add all mandatory attributes
            info.set(X509CertInfo.VERSION, new CertificateVersion(CertificateVersion.V3));
            info.set(X509CertInfo.SERIAL_NUMBER, new CertificateSerialNumber(new java.util.Random().nextInt() & 0x7fffffff));
            AlgorithmId algID = AlgorithmId.getAlgorithmId(sigAlg);
            info.set(X509CertInfo.ALGORITHM_ID, new CertificateAlgorithmId(algID));
           // info.set(X509CertInfo.SUBJECT, new CertificateSubjectName(myname));
            info.set(X509CertInfo.SUBJECT,myname);
            info.set(X509CertInfo.KEY, new CertificateX509Key(publicKey));
            info.set(X509CertInfo.VALIDITY, interval);
           // info.set(X509CertInfo.ISSUER, new CertificateIssuerName(myname));
            info.set(X509CertInfo.ISSUER,myname);

            cert = new X509CertImpl(info);
            cert.sign(privateKey, this.sigAlg);

            return (X509Certificate) cert;

        } catch (IOException e) {
            throw new CertificateEncodingException("getSelfCert: " +
                    e.getMessage());
        }
    }

    // Keep the old method
    public X509Certificate getSelfCertificate(X500Name myname, long validity) throws CertificateException, InvalidKeyException, SignatureException, NoSuchAlgorithmException, NoSuchProviderException {
        return getSelfCertificate(myname, new Date(), validity);
    }

    public PKCS10 getCertRequest(X500Name myname) throws InvalidKeyException, SignatureException {
        PKCS10 req = new PKCS10(publicKey);

        try {
            Signature signature = Signature.getInstance(sigAlg);
            signature.initSign(privateKey);
            req.encodeAndSign(myname, signature);

        } catch (CertificateException e) {
            throw new SignatureException(sigAlg + " CertificateException");

        } catch (IOException e) {
            throw new SignatureException(sigAlg + " IOException");

        } catch (NoSuchAlgorithmException e) {
            // "can't happen"
            throw new SignatureException(sigAlg + " unavailable?");
        }
        return req;
    }

    private SecureRandom prng;
    private String sigAlg;
    private KeyPairGenerator keyGen;
    private PublicKey publicKey;
    private PrivateKey privateKey;
}