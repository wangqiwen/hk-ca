package com.hwking.hkca.ssl;

import java.io.PrintStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.cert.CertificateException;
import java.security.NoSuchAlgorithmException;
import java.security.InvalidKeyException;
import java.security.Signature;
import java.security.SignatureException;
import java.security.PublicKey;

import sun.misc.BASE64Encoder;
import sun.security.pkcs10.PKCS10Attributes;
import sun.security.util.DerInputStream;
import sun.security.util.DerOutputStream;
import sun.security.util.DerValue;
import sun.security.x509.AlgorithmId;
import sun.security.x509.X509Key;
import sun.security.x509.X500Name;

/**
 * @author wangqiwen
 */
public class PKCS10 {

    public PKCS10(PublicKey publicKey) {
        subjectPublicKeyInfo = publicKey;
        attributeSet = new PKCS10Attributes();
    }


    public PKCS10(PublicKey publicKey, PKCS10Attributes attributes) {
        subjectPublicKeyInfo = publicKey;
        attributeSet = attributes;
    }

    public PKCS10(byte[] data) throws IOException,SignatureException,NoSuchAlgorithmException

    {
         DerInputStream in;
         DerValue[] seq;
         AlgorithmId id;
         byte[] sigData;
         Signature sig;

         encoded = data;

                   //
                   // Outer sequence:  request, signature algorithm, signature.
                   // Parse, and prepare to verify later.
                   //
         in = new DerInputStream(data);
         seq = in.getSequence(3);

         if (seq.length != 3)
         throw new IllegalArgumentException("not a PKCS #10 request");

         data = seq[0].toByteArray();            // reusing this variable
         id = AlgorithmId.parse(seq[1]);
         sigData = seq[2].getBitString();

                   //
                   // Inner sequence:  version, name, key, attributes
                   //
         BigInteger serial;
         DerValue val;

         serial = seq[0].data.getBigInteger();
         if (!serial.equals(BigInteger.ZERO))
         throw new IllegalArgumentException("not PKCS #10 v1");

         subject = new X500Name(seq[0].data);
         subjectPublicKeyInfo = X509Key.parse(seq[0].data.getDerValue());

                   // Cope with a somewhat common illegal PKCS #10 format
         if (seq[0].data.available() != 0)
         attributeSet = new PKCS10Attributes(seq[0].data);
                   else
         attributeSet = new PKCS10Attributes();

         if (seq[0].data.available() != 0)
         throw new IllegalArgumentException("illegal PKCS #10 data");

                   //
                   // OK, we parsed it all ... validate the signature using the
                   // key and signature algorithm we found.
                   //
         try {
         sig = Signature.getInstance(id.getName());
         sig.initVerify(subjectPublicKeyInfo);
         sig.update(data);
         if (!sig.verify(sigData))
             throw new SignatureException("Invalid PKCS #10 signature");

    } catch (InvalidKeyException e) {
         throw new SignatureException("invalid key");

    }

    }


    public void encodeAndSign(X500Name subject, Signature signature) throws CertificateException,IOException,SignatureException

    {
         DerOutputStream out, scratch;
         byte[] certificateRequestInfo;
         byte[] sig;

         if (encoded != null)
         throw new SignatureException("request is already signed");

         this.subject = subject;

                   /*
             * Encode cert request info, wrap in a sequence for signing
              */
         scratch = new DerOutputStream();
         scratch.putInteger(BigInteger.ZERO);            // PKCS #10 v1.0
         subject.encode(scratch);                        // X.500 name
         scratch.write(subjectPublicKeyInfo.getEncoded()); // public key
         attributeSet.encode(scratch);

         out = new DerOutputStream();
         out.write(DerValue.tag_Sequence, scratch);      // wrap it!
         certificateRequestInfo = out.toByteArray();
         scratch = out;

                   /*
              * Sign it ...
              */
         signature.update(certificateRequestInfo, 0, certificateRequestInfo.length);
         sig = signature.sign();

                   /*
              * Build guts of SIGNED macro
              */
         AlgorithmId algId = null;
         try {
         algId = AlgorithmId.getAlgorithmId(signature.getAlgorithm());

    } catch (NoSuchAlgorithmException nsae) {
         throw new SignatureException(nsae);

    }
         algId.encode(scratch);     // sig algorithm
         scratch.putBitString(sig);                      // sig

                   /*
              * Wrap those guts in a sequence
              */
         out = new DerOutputStream();
         out.write(DerValue.tag_Sequence, scratch);
         encoded = out.toByteArray();

    }


    public X500Name getSubjectName() {
        return subject;
    }


    public PublicKey getSubjectPublicKeyInfo() {
        return subjectPublicKeyInfo;
    }


    public PKCS10Attributes getAttributes() {
        return attributeSet;
    }


    public byte[] getEncoded() {
         if (encoded != null)
             return encoded.clone();
                   else
         return null;

    }

    public void print(PrintStream out) throws IOException,SignatureException

    {
         if (encoded == null)
         throw new SignatureException("Cert request was not signed");

         BASE64Encoder encoder = new BASE64Encoder();

         out.println("-----BEGIN NEW CERTIFICATE REQUEST-----");
         encoder.encodeBuffer(encoded, out);
         out.println("-----END NEW CERTIFICATE REQUEST-----");

    }


    public String toString() {
         return "[PKCS #10 certificate request:\n"
         + subjectPublicKeyInfo.toString()
         + " subject: <" + subject + ">" + "\n"
         + " attributes: " + attributeSet.toString()
         + "\n]";

    }

    public boolean equals(Object other) {
         if (this == other)
             return true;
         if (!(other instanceof PKCS10))
             return false;
         if (encoded == null) // not signed yet
             return false;
         byte[] otherEncoded = ((PKCS10) other).getEncoded();
         if (otherEncoded == null)
             return false;

         return java.util.Arrays.equals(encoded, otherEncoded);

    }


    public int hashCode() {
         int retval = 0;
         if (encoded != null)
             for (int i = 1; i < encoded.length; i++)
             retval += encoded[i] * i;
         return (retval);

    }

    private X500Name subject;
    private PublicKey subjectPublicKeyInfo;
    private PKCS10Attributes attributeSet;
    private byte[] encoded;        // signed
}