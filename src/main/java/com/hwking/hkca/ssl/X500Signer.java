package com.hwking.hkca.ssl;

import sun.security.x509.AlgorithmId;
import sun.security.x509.X500Name;

import java.security.Signature;
import java.security.SignatureException;
import java.security.Signer;
import java.security.NoSuchAlgorithmException;

public class X500Signer extends Signer {
    private static final long serialVersionUID = -8609982645394364834L;

    public void update(byte buf[], int offset, int len) throws SignatureException {
        sig.update(buf, offset, len);
    }

    public byte[] sign() throws SignatureException {
         return sig.sign();

    }

    public AlgorithmId getAlgorithmId() {
         return algid;

    }


    public X500Name getSigner() {
         return agent;

    }

    public X500Signer(Signature sig, X500Name agent) {
         if (sig == null || agent == null)
             throw new IllegalArgumentException("null parameter");

         this.sig = sig;
         this.agent = agent;

         try {
             this.algid = AlgorithmId.getAlgorithmId(sig.getAlgorithm());


        } catch (NoSuchAlgorithmException e) {
             throw new RuntimeException("internal error! " + e.getMessage());

        }

    }


    private Signature sig;

    private X500Name agent;          // XXX should be X509CertChain

    private AlgorithmId algid;

}

