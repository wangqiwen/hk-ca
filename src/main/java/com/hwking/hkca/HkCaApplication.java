package com.hwking.hkca;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HkCaApplication {

	public static void main(String[] args) {
		SpringApplication.run(HkCaApplication.class, args);
	}

}
